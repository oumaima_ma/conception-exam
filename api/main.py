from fastapi import FastAPI
import uvicorn
import requests
from pydantic import BaseModel



app = FastAPI()

class Deck(BaseModel):
    deck_id: str


@app.get("/")
def read_root():
    return {"Hello": "World"}

requete = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
print(requete.status_code)
print(requete.json())


@app.get("/creer-un-deck/")
def get_id():
    """fonction qui récupère l'id du deck """
    requete = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    deck_id = requete.json()["deck_id"]
    return deck_id

@app.post("/cartes/{nombre_cartes}")
def draw_card(nombre_cartes: int, deck: Deck):
    """fonction qui qui pioche une carte"""

    requete = requests.get("https://deckofcardsapi.com/api/deck/" + deck.deck_id + "/draw/?count=" +
                           str(nombre_cartes))
    res = requete.json()
    return(res)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)


