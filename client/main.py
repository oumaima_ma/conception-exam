import requests

class fonctions:
    #fct du creation du deck qui renvoie son id
    def get_id():
        r=requests.get("http://127.0.0.1:8000/creer-un-deck/")
        res=r.json()
        return {"deck_id": res}

    #fct qui tire un nombre choisi de cartes depuis le deck déjà créé
    def draw_card(Deck, nombre_cartes):
        str_nombre_cartes = "{}".format(nombre_cartes)
        r=requests.post("http://127.0.0.1:8000/cartes/"+str_nombre_cartes, json=Deck)
        res=r.json()
        return {"deck_id" : res["deck_id"], "remaining" : res["remaining"], "cards":res["cards"]}

    #fonction qui calcule le nombre de chaque couleurs dans les cartes tirées
    def count_colors(card_list):
        length = len(card_list)
        hearts=0
        spades=0
        diamonds=0
        clubs=0
        for i in range(0,length):
            if card_list[i]["suit"] == "HEARTS":
                hearts +=1
            elif card_list[i]["suit"] == "SPADES":
                spades +=1
            elif card_list[i]["suit"] == "DIAMONDS":
                diamonds +=1
            else :
                clubs += 1     
        colors = {"H":hearts, "S" : spades, "C" : clubs, "D" : diamonds}
        return colors

#lancer un scénario où l'utilisateur choisis le nombre de cartes tirées
if __name__ == "__main__":
    print("let's play a card game, are you ready? :D")
    # Initialisation du deck
    deck = fonctions.get_id()
    print("The deck's id is : {} \n".format(deck["deck_id"]))
    # piocher des cartes
    print("Choose the number of card you want to draw")
    # l'utilisateur entre le nombre de cartes qu'il veut piocher
    nombre_cartes = input("You chose to draw : ")
    drawn_cards = fonctions.draw_card(deck,nombre_cartes)
    #calcul des cartes tirée dans chaque couleur 
    cardList = drawn_cards["cards"]   
    colors = fonctions.count_colors(cardList)
    print("Here's the number of cards in each color: {}\n".format(colors))

