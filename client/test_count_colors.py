from main import fonctions
import json

#On teste la fonction de calcul de couleurs pour un exemple de liste de cartes
def test_count_colors():
    #Exemple de 4 cartes mis dans un fichier json pour tester la fonction
    with open('client/test_scenario.json') as json_data:  
        scenario = json.load(json_data) 
    #Le résultat attendu si la fonction est correcte
    assert fonctions.count_colors(scenario) == {"H": 0, "S": 1, "D": 2, "C": 1} 
